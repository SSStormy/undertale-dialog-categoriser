# Undertale dialog categoriser
A pretty rough python scripts that attempt to categorise dialog from Undertale by character and optionally print the script it came from.

It is by no means accurate but it does a good enough job. If you want to find side character dialog, I recommend looking into full.txt and searching for the name of the character. Chances are, the script which contains the dialog will be named after the character.

It has been designed around the decompiled code (-c argument) provided by [Altar.NET](https://gitlab.com/PoroCYon/Altar.NET)

You can find the categorised text files in the `/text` folder of the repo.

## Usage

###### Help message as of 1.0.0
```
*** undertale-dialog-organiser v1.0.0.***
* Licensed under the MIT license.
* https://github.com/SSStormy/undertale-dialog-categoriser
---
-h --help          Prints this help message.
-v --verbose       Displays verbose output to stdout.
-d --dir           (REQUIRED) The directory of the folder containing Undertale scripts decompiled by Altar.NET.
-f --file          Directory of the file to which message data will be written to.
-p --pipe          Writes message data to stdout.
-s --separate      The directory of the folder in which we will write to a separate file for each message group.
-c --categorise    Categorise messages by the file they appear in. Specify the 'empty' argument to only to this to the empty category, not-empty' to do this to every group except empty, 'all' to do this for all groups.
-l --list          Lists all message groups.
Input directory not specified. Use -d or -h for more info.
```

### License
MIT
