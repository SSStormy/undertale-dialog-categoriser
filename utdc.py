#!/usr/bin/python

import sys, os, getopt
from enum import Enum
from os import path

class Group(Enum):
    Unset =   -1  # no group was set. if current_group == Unset, add msg to stog_buffer
    Empty =    0  # doesn't display a face sprite.
    Toriel =   1
    Flowey =   2
    Sans =     3
    Papyrus =  4
    Undyne =   5
    Alphys =   6
    Asgore =   7
    Mettaton = 8
    Asriel =   9

class Message(object):
    def __init__(self, text, filename):
        self.text = text
        self.filename = filename

__version__ = "1.0.0"

dialogs = {}

FACECHOICE_IDENTIFIER = "global.facechoice = "
FACECALL_IDENTIFIER   = "call (scr_"
STOGMSG_IDENTIFIER    = "stog.msg["

def parse_facechoice(raw_line):
    raw_line = raw_line.replace(FACECHOICE_IDENTIFIER, "").replace("s", "")
    return Group(int(raw_line))

def parse_facecall(raw_line):
    raw_line = raw_line.replace("face", "") # remove the face thingy

    if raw_line == "tor":
        return Group.Toriel
    elif raw_line == "al":
        return Group.Alphys
    elif raw_line == "und":
        return Group.Undyne
    elif raw_line == "sans":
        return Group.Sans
    elif raw_line == "pap":
        return Group.Papyrus
    elif raw_line == "asg":
        return Group.Asgore
    else:
        print("Unknown facecall: " + raw_line)


def parse_data(file_dir):
    stog_buffer = [] # holds messages if current_group is Group.Unset. We add everything from it to dialogs when we reach EOF.
    current_group = Group.Unset

    with open(file_dir) as lsp_file:
        for line in lsp_file.read().splitlines():
            line = line.strip() # remove indentation

            # ---- look for group ---- #
            ## look for global.facechoice

            if line.startswith(FACECHOICE_IDENTIFIER):

                if verbose:
                    print("-- Found facechoice")
                    print("File   : " + path.basename(file_dir))
                    print("Raw    : " + line)

                try:
                    current_group =  parse_facechoice(line)
                    if verbose:
                        print("Parsed : " + str(current_group))
                except ValueError as e:
                    if verbose:
                        print("*** Failed parsing facechoice due to ValueError.***")
                        print("Exception : " + str(e))

            ## look for "call (scr_GROUPface[]"

            elif line.startswith(FACECALL_IDENTIFIER):

                # verify that this is infact a facecall
                line = line.replace(FACECALL_IDENTIFIER, "") #remove the facecall identifier
                line = line[:line.index("[")] # remove everything up until the [] part of the call.

                if line.endswith("face"):
                    current_group = parse_facecall(line)

                    if verbose:
                        print("-- Found facecall")
                        print("File   : " + path.basename(file_dir))
                        print("Raw    : " + line)
                        print("Parsed : " + str(current_group))

            # ---- look for stog.msg ---- #

            elif line.startswith(STOGMSG_IDENTIFIER):
                # remove everything up until the first quote and remove the last character.
                quote_index = line.find("\"")

                if not quote_index == -1:
                    text = line[line.index("\"")+1 :-1]

                    if current_group == Group.Unset:
                        stog_buffer.append(text)
                    else:
                        add_msg_to_dialog(current_group, Message(text, file_dir))
                        if verbose:
                            print("-- Found stog.msg")
                            print("File   : " + path.basename(file_dir))
                            print("Raw    : " + line)
                            print("Parsed : " + text)

    for buf_text in stog_buffer:
       add_msg_to_dialog(Group.Empty, Message(buf_text, file_dir))

def add_msg_to_dialog(group, message):
    if not group in dialogs:
        dialogs[group] = []

    dialogs[group].append(message)

def print_help():
    print("*** undertale-dialog-organiser v%s.***" % __version__)
    print("* Licensed under the MIT license.")
    print("* https://github.com/SSStormy/undertale-dialog-categoriser")
    print("---")
    print("%-18s %s" % ("-h --help", "Prints this help message."))
    print("%-18s %s" % ("-v --verbose", "Displays verbose output to stdout."))
    print("%-18s %s" % ("-d --dir", "(REQUIRED) The directory of the folder containing Undertale scripts decompiled by Altar.NET."))
    print("%-18s %s" % ("-f --file", "Directory of the file to which message data will be written to."))
    print("%-18s %s" % ("-p --pipe", "Writes message data to stdout."))
    print("%-18s %s" % ("-s --separate", "The directory of the folder in which we will write to a separate file for each message group."))
    print("%-18s %s" % ("-c --categorise", "Categorise messages by the file they appear in. Specify the '%s' argument to only to this to the empty category, %s' to do this to every group except empty, '%s' to do this for all groups." % (cat_empty, cat_not_empty, cat_all)))
    print("%-18s %s" % ("-l --list", "Lists all message groups."))

cat_empty     = "empty"
cat_not_empty = "not-empty"
cat_all       = "all"

input_dir        = None    # the directory from which we will get all our script data from
out_file_dir     = None    # the file to which we will dump all messages
pipe_flag        = False   # a flag determining whether we'll have to dump messages into stdout
out_folder       = None    # the folder of the seperate file folder.
sort_by_file_cat = "empty" # how we will sort the messages by the file they appear in.
verbose          = False

def handle_args(arguments):
    # python why
    global input_dir
    global out_file_dir
    global pipe_flag
    global out_folder
    global sort_by_file_cat
    global verbose

    arguments = arguments[1:]

    if not arguments: # no args given
        print_help()

    try:
        opts, args = getopt.getopt(arguments, "hvd:f:ps:c:l", ["help", "verbose", "dir=", "file=", "pipe", "separate=", "categorise=", "list"])
    except getopt.GetoptError as e:
        print("Failed parsing arguments: " + str(e))
        sys.exit(1)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print_help()

        elif opt in ("-v", "--verbose"):
            verbose = True

        elif opt in ("-d", "--dir"):
            if path.exists(arg):
                input_dir = arg
            else:
                print("(-d) Path not found: " + arg)
                sys.exit(1)

        elif opt in ("-f", "--file"):
            if os.access(path.dirname(arg), os.W_OK):
                out_file_dir = arg
            else:
                print("(-f) Can't access " + arg + ". No write permissions? Invalid dir?")
                sys.exit(1)

        elif opt in ("-p", "--pipe"):
            pipe_flag = True

        elif opt in ("-s", "--separate"):
            if path.exists(arg):
                out_folder = arg
            else:
                print("(-s) Separate out folder dir not found")

        elif opt in ("-c", "--categorise"):
            if arg not in (cat_empty, cat_not_empty, cat_all):
                print("(-c) Invalid categorise category "  + arg)
                sys.exit(1)
            else:
                sort_by_file_cat = arg


        elif opt in ("-l", "--list"):
            for cat in Group:
                print(str(cat.value) + ") " + cat.name)

        else:
            print("Unknown argument: " + opt)

def parse_files():
    for f in os.listdir(input_dir):
        if f.endswith(".gml.lsp"):
            parse_data(path.join(input_dir, f))


def cat_by_file(msglist):
    for msg in msglist:
        msg.text = "%-90s %s" % (msg.text, path.basename(msg.filename))

def handle_output():
    # categorise output
    if sort_by_file_cat == cat_empty:
        if Group.Empty in dialogs:
            cat_by_file(dialogs[Group.Empty])

    elif sort_by_file_cat == cat_not_empty:
        for group, msgs in dialogs.items():
            if group != Group.Empty:
                cat_by_file(msgs)

    elif sort_by_file_cat == cat_all:
        for msgs in dialogs.values():
            cat_by_file(msgs)

    if pipe_flag:
        for group, msgs in dialogs.items():
            print("==> " + group.name)
            for msg in msgs:
                print(msg.text)

    if out_file_dir:
        with open(out_file_dir, "w+") as out_file:
            for group, msgs in dialogs.items():
                out_file.write("==> " + group.name + os.linesep)
                for msg in msgs:
                    out_file.write(msg.text + os.linesep)

    if out_folder:
        for group, msgs in dialogs.items():
            with open(path.join(out_folder, group.name + ".txt"), "w+") as out_g_file:
                out_g_file.write("==> " + group.name + os.linesep)
                for msg in msgs:
                    out_g_file.write(msg.text + os.linesep)

if __name__ == "__main__":
    handle_args(sys.argv)

    if not input_dir:
        print("Input directory not specified. Use -d or -h for more info.")
        sys.exit(1)

    parse_files()
    handle_output()
